/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class ComboBoxExample1 {

    JFrame frame;

    ComboBoxExample1() {
        frame = new JFrame("ComboBox Example");
        String sport[] = { "basketball", "football", "volleyball", "tennis", "badminton" };
        JComboBox cb = new JComboBox(sport);
        cb.setBounds(50, 50, 95, 25);

        frame.add(cb);
        frame.setLayout(null);
        frame.setSize(400, 500);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new ComboBoxExample1();
    }
}
