/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
public class TextAreaExample2 implements ActionListener {

    JLabel la1, la2;
    JTextArea area;
    JButton button;

    TextAreaExample2() {
        JFrame frame = new JFrame();
        la1 = new JLabel();
        la1.setBounds(50, 25, 100, 30);
        la2 = new JLabel();
        la2.setBounds(160, 25, 100, 30);

        area = new JTextArea();
        area.setBounds(20, 70, 250, 200);

        button = new JButton("Count Words");
        button.setBounds(100, 300, 120, 30);
        button.addActionListener(this);
        frame.add(la1);
        frame.add(la2);
        frame.add(area);
        frame.add(button);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        la1.setText("Word: " + words.length);
        la2.setText("Characters: " + text.length());
    }

    public static void main(String[] args) {
        new TextAreaExample2();
    }

}
