/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class TextFieldExmple1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("TextField Example");
        JTextField tx1, tx2;
        tx1 = new JTextField("Welcome to Javatpoint");
        tx1.setBounds(50, 100, 200, 30);

        tx2 = new JTextField("AWT Tutorial");
        tx2.setBounds(50, 150, 200, 30);
        frame.add(tx1);
        frame.add(tx2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

    }
    
}
