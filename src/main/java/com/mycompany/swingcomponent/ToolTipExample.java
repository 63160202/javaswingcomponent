/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class ToolTipExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        //Creating PasswordField and label  
        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("Enter your Password");
        JLabel l1 = new JLabel("Password:");
        l1.setBounds(20, 100, 80, 30);
        //Adding components to frame  
        frame.add(value);
        frame.add(l1);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
