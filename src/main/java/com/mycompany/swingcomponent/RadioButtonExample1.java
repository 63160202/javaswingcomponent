/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class RadioButtonExample1 {

    JFrame frame;

    RadioButtonExample1() {
        frame = new JFrame();
        JRadioButton rb1 = new JRadioButton("A) Male");
        rb1.setBounds(75, 50, 100, 30);

        JRadioButton rb2 = new JRadioButton("B) Female");
        rb2.setBounds(75, 100, 100, 30);

        ButtonGroup bg = new ButtonGroup();
        bg.add(rb1);
        bg.add(rb2);

        frame.add(rb1);
        frame.add(rb2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        new RadioButtonExample1();
    }
}
