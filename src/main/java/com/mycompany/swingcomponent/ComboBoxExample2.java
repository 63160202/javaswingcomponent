/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
public class ComboBoxExample2 {

    JFrame frame;

    ComboBoxExample2() {
        frame = new JFrame("ComboBox Example");
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);

        JButton button = new JButton("Show");
        button.setBounds(200, 100, 75, 20);

        String Ashes[] = { "basketball", "football", "volleyball", "tennis", "badminton" };
        final JComboBox cb = new JComboBox(Ashes);
        cb.setBounds(50, 100, 90, 20);

        frame.add(cb);
        frame.add(label);
        frame.add(button);
        frame.setLayout(null);
        frame.setSize(350, 350);
        frame.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "your chosen sport: " + cb.getItemAt(cb.getSelectedIndex());
                label.setText(data);
            }
        });
    }

    public static void main(String[] args) {
        new ComboBoxExample2();
    }
}
