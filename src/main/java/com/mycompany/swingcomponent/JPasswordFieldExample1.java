/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class JPasswordFieldExample1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        JPasswordField value = new JPasswordField();
        JLabel la1 = new JLabel("Password");
        la1.setBounds(20, 100, 80, 30);
        value.setBounds(100, 100, 100, 30);
        frame.add(value);
        frame.add(la1);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

    }
}
