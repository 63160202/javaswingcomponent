/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class FirstSwingExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        JButton button = new JButton("Click");
        button.setBounds(90, 90, 100, 30);

        frame.add(button);

        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
