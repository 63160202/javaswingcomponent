/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
public class CheckBoxExample3 extends JFrame implements ActionListener {

    JLabel label;
    JCheckBox cb1, cb2, cb3;
    JButton button;

    CheckBoxExample3() {
        label = new JLabel("Food Ordering System");
        label.setBounds(50, 50, 300, 20);

        cb1 = new JCheckBox("donut @ 50");
        cb1.setBounds(100, 100, 150, 20);

        cb2 = new JCheckBox("cupcake @ 60");
        cb2.setBounds(100, 150, 150, 20);

        cb3 = new JCheckBox("coffee @ 40");
        cb3.setBounds(100, 200, 150, 20);

        button = new JButton("Order");
        button.setBounds(100, 250, 80, 30);
        button.addActionListener(this);

        add(label);
        add(cb1);
        add(cb2);
        add(cb3);
        add(button);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        float amount = 0;
        String msg = "";
        if (cb1.isSelected()) {
            amount += 50;
            msg = "donut: 50\n";
        }
        if (cb2.isSelected()) {
            amount += 60;
            msg += "cupcake: 60\n";
        }
        if (cb3.isSelected()) {
            amount += 40;
            msg += "coffee: 40\n";
        }
        msg += "-----------------\n";
        JOptionPane.showMessageDialog(this, msg + "Total: " + amount);
    }

    public static void main(String[] args) {
        new CheckBoxExample3();
    }
}
