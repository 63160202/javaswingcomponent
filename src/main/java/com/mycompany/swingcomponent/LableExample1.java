/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class LableExample1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Label Example");
        JLabel la1, la2;

        la1 = new JLabel("we love nct");
        la1.setBounds(50, 50, 150, 30);

        la2 = new JLabel("nct127 & nctdream & wayv");
        la2.setBounds(50, 100, 250, 30);

        frame.add(la1);
        frame.add(la2);
        frame.setSize(250, 250);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
