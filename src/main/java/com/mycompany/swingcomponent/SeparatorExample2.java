/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
public class SeparatorExample2 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("Separator Example");
        frame.setLayout(new GridLayout(0, 1));
        JLabel l1 = new JLabel("Above Separator");
        frame.add(l1);
        JSeparator sep = new JSeparator();
        frame.add(sep);
        JLabel l2 = new JLabel("Below Separator");
        frame.add(l2);
        frame.setSize(400, 100);
        frame.setVisible(true);
    }
}
