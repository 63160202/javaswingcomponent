/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
public class RadioButtonExample2 extends JFrame implements ActionListener {

    JRadioButton rb1, rb2;
    JButton button;

    RadioButtonExample2() {
        rb1 = new JRadioButton("Male");
        rb1.setBounds(100, 50, 100, 30);

        rb2 = new JRadioButton("Female");
        rb2.setBounds(100, 100, 100, 30);

        ButtonGroup bg = new ButtonGroup();
        bg.add(rb1);
        bg.add(rb2);

        button = new JButton("click");
        button.setBounds(100, 150, 80, 30);
        button.addActionListener(this);

        add(rb1);
        add(rb2);
        add(button);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (rb1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Male.");
        }
        if (rb2.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Female.");
        }
    }

    public static void main(String[] args) {
        new RadioButtonExample2();
    }
}
