/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
public class ButtonExample2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button");
        final JTextField textb = new JTextField();

        textb.setBounds(65, 50, 150, 30);

        JButton button = new JButton("Click Here");
        button.setBounds(90, 90, 100, 30);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textb.setText("Welcome to Javatpoint");
            }
            
        });

        frame.add(button);
        frame.add(textb);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

    }
}

