/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author admin
 */
public class TextFieldExmple2 implements ActionListener {

    JTextField tx1, tx2, tx3;
    JButton button1, button2;

    TextFieldExmple2() {
        JFrame frame = new JFrame();
        tx1 = new JTextField();
        tx1.setBounds(50, 50, 150, 20);
        tx2 = new JTextField();
        tx2.setBounds(50, 100, 150, 20);
        tx3 = new JTextField();
        tx3.setBounds(50, 150, 150, 20);
        tx3.setEditable(false);
        button1 = new JButton("+");
        button1.setBounds(50, 200, 50, 50);
        button2 = new JButton("-");
        button2.setBounds(120, 200, 50, 50);
        button1.addActionListener(this);
        button2.addActionListener(this);
        frame.add(tx1);
        frame.add(tx2);
        frame.add(tx3);
        frame.add(button1);
        frame.add(button2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String st1 = tx1.getText();
        String st2 = tx2.getText();
        int a = Integer.parseInt(st1);
        int b = Integer.parseInt(st2);
        int c = 0;
        if (e.getSource() == button1) {
            c = a + b;
        } else if (e.getSource() == button2) {
            c = a - b;
        }
        String result = String.valueOf(c);
        tx3.setText(result);
    }

    public static void main(String[] args) {
        new TextFieldExmple2();
    }

}
