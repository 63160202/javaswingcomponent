/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.*;

/**
 *
 * @author admin
 */
class IconExample1 {

    IconExample1() {
        Frame f = new Frame();
        Image icon = Toolkit.getDefaultToolkit().getImage("C:\\ldk\\icon1.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(300, 300);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new IconExample1();
    }
}
