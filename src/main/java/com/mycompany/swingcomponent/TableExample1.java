/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class TableExample1 {

    JFrame f;

    TableExample1() {
        f = new JFrame();
        String data[][] = {{"201", "Mark", "670000"},
        {"202", "Jay", "780000"},
        {"203", "Ten", "700000"}};
        String column[] = {"ID", "NAME", "SALARY"};
        JTable jt = new JTable(data, column);
        jt.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(jt);
        f.add(sp);
        f.setSize(300, 400);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new TableExample1();
    }
}
