/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author admin
 */
class MyJComponent extends JComponent {

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        g.fillRect(30, 30, 100, 100);
    }
}

public class JComponentExample {

    public static void main(String[] arguments) {
        MyJComponent com = new MyJComponent();
        // create a basic JFrame
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // add the JComponent to main frame
        frame.add(com);
        frame.setVisible(true);
    }
}
