/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;

/**
 *
 * @author admin
 */
public class OptionPaneExample2 {

    JFrame frame;

    OptionPaneExample2() {
        frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "Successfully Updated.", "Alert", JOptionPane.WARNING_MESSAGE);
    }

    public static void main(String[] args) {
        new OptionPaneExample2();
    }
}
